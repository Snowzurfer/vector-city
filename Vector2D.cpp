/**
 *  Vector2D.cpp
 *  Source file of the Vector2D class
 *  
 *  Contains the definition of member functions
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

 // Included libs
#include "Vector2D.h"

Vector2D::Vector2D(f16 x, f16 y)
{
	this->x = x; this->y = y;
	
	// Calculate the magnitude
	magnitude = sqrt((x*x) + (y*y));
}

Vector2D::~Vector2D(){}

void Vector2D::rotate2D(f16 angle)
{
	// Store the trigs of the angle, to save CPU (they won't be computed again)
	f16 cos_angle = fixedpoint::cos(f16(angle));
	f16 sin_angle = fixedpoint::sin(f16(angle));
	
	// Rotate the direction around the origin
	f16 temp_x = x*cos_angle - y*sin_angle;
	f16 temp_y = x*sin_angle + y*cos_angle;
	
	// Assign values back
	x = temp_x; y = temp_y;
	
	// Change angle
	this->angle += angle;
}

void Vector2D::setPos(f16 new_x, f16 new_y)
{
	this->x = new_x; this->y = new_y;	
}

void Vector2D::addToPos(f16 x_step, f16 y_step)
{
	// Add values
	x += x_step; y += y_step;
}

f16 Vector2D::dotProduct(const Vector2D &b)
{
	return ((x * b.x) + (y * b.y));
}

void Vector2D::normalize()
{
	if (magnitude != 0.0)
	{
		x = x / magnitude;
		y = y / magnitude;
	}
}

void Vector2D::makeNormalLeft()
{
	// Save the original x (i) component
	f16 temp_x = x;
	// Perform the change
	x = y;
	y =  -1 * temp_x;	
}

