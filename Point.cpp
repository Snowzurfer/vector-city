/**
 *  Point.cpp
 *  Source file of the Point class.
 *  
 *  Contains the definition of member functions
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

// Included libs
#include "Point.h"
Point::Point()
{
	x = 0; y = 0;
}


Point::Point(f16 x, f16 y)
{
	this->x = x; this->y = y;
}

Point::~Point(){}

void Point::rotate2D(f16 angle)
{
	// Store the trigs of the angle, to save CPU (they won't be computed again)
	f16 cos_angle = fixedpoint::cos(f16(angle));
	f16 sin_angle = fixedpoint::sin(f16(angle));
	
	// Rotate the position around the origin
	f16 temp_x = x*cos_angle - y*sin_angle;
	f16 temp_y = x*sin_angle + y*cos_angle;
	
	// Assign values back
	x = temp_x; y = temp_y;
}

void Point::setPos(f16 new_x, f16 new_y)
{
	this->x = new_x; this->y = new_y;	
}

void Point::addToPos(f16 x_step, f16 y_step)
{
	// Add values
	x += x_step; y += y_step;
}

