/*
	This lib contains methods which come in
	handy during the GBA development. It can be
	considered as an expansion of the gba.h library
	by Adam Sampson.
	
	Coded by Alberto Taiuti
*/

#ifndef UTILS_H
#define UTILS_H

// Included libs
#include "font.h"
#include "gba.h"
#include <string.h>
#include <cstdlib>
#include <cstdio>
#include "fixed_class.h"

// 	Define some colours
const uint16_t red = RGB(31, 0, 0);
const uint16_t green = RGB(0, 31, 0);
const uint16_t blue = RGB(0, 0, 31);
const uint16_t white = RGB(31, 31, 31);
const uint16_t bright_pink = RGB(31, 15, 29);
const uint16_t black = RGB(0, 0, 0);
const uint16_t yellow = RGB(31, 31, 0);

// Define a type for the 16.16 fixed point type for ease of writing
typedef fixedpoint::fixed_point<16> f16;

// Draw a line using Bresenham's algorithm, with a given colour (col)
void bresenhamLine8(f16 x1, f16 y1, f16 x2, f16 y2, int col);

// Check whether a certain button has been pressed
bool isBPressed(uint16_t button, uint16_t reg = REG_KEYINPUT);

// Check whether a certain button has JUST been pressed
bool isBJustPressed(uint16_t button, uint16_t prevReg, uint16_t reg = REG_KEYINPUT);
	
// Draw ASCII char using the font.h library at at given position (x, y)
void drawChar8(int32_t x, int32_t y, int32_t colour, char c, bool bold);
	
// Draw \0-terminated string at a given position (x, y) in a given colour (colour)
void drawString8(int32_t x, int32_t y, int32_t colour, const char *s, bool bold);


/**
 * Draw a circular explosion at a given position(x, y),
 * with given radius (radius) and number
 * of particles (part_num)
 */
void drawExplosion(int32_t x, int32_t y, int32_t part_num, int32_t radius, int32_t col);

#endif