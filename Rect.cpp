/**
 *  Rect.cpp
 *  Source file of the Rect class.
 *  
 *  Contains the definition of member functions.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

// Included libs
#include "Rect.h"


Rect::Rect(f16 centerX, f16 centerY, f16 _width, f16 _height) :
			width(_width), height(_height), half_width(_width / 2), half_height (height / 2)
{
	points.push_back(new Point(centerX, centerY)); // Centre. Global coordinates
	points.push_back(new Point( -half_width, -half_height)); // Top left vertex
	points.push_back(new Point( half_width, -half_height)); // Top right vertex
	points.push_back(new Point( -half_width, half_height)); // Bottom left vertex
	points.push_back(new Point( half_width, half_height)); // Bottom right vertex
}

Rect::~Rect()
{
	// Delete point instances
	points.clear();
}

void Rect::drawFrame8(coloursPalPos col)
{
	// Draw the shape of the car using the Bresenham's algorithm
	bresenhamLine8(points[1]->x + points[0]->x, points[1]->y + points[0]->y, points[2]->x + points[0]->x, points[2]->y+ points[0]->y, col);
	bresenhamLine8(points[1]->x + points[0]->x, points[1]->y + points[0]->y, points[3]->x + points[0]->x, points[3]->y + points[0]->y, col);
	bresenhamLine8(points[3]->x + points[0]->x, points[3]->y + points[0]->y, points[4]->x + points[0]->x, points[4]->y + points[0]->y, col);
	bresenhamLine8(points[2]->x + points[0]->x, points[2]->y + points[0]->y, points[4]->x + points[0]->x, points[4]->y + points[0]->y, col);
}

void Rect::rotatePoints(f16 angle)
{
	// For each vertex
	for(int32_t i = 1; i <= 4; i++)
	{
		// Rotate
		points[i]->rotate2D(angle);
	}
}

void Rect::addToCentrePosition(f16 x, f16 y)
{
	// Change the position of the centre (move it) by (x, y)
	points[0]->addToPos(x, y);
}

Point Rect::getPointGlobal(int32_t point_num)
{
	return (Point(points[point_num]->x + points[0]->x, points[point_num]->y + points[0]->y));
}

std::vector<Vector2D> Rect::getNormalsAsArray()
{
	// Create an array containing the normals
	std::vector<Vector2D> normals;
	
	// Iterate through the points array, starting from the 2nd (as 1st is centre)
	for(uint32_t i = 1; i < points.size() - 1; i++)
	{
		// Save a temporary vector
		Vector2D temp_vect = Vector2D(points[i+1]->x - points[i]->x, points[i+1]->y - points[i]->y);
		
		// Make it become a left normal
		temp_vect.makeNormalLeft();
		
		// Push it into the array
		normals.push_back(temp_vect);
	}
	
	// The last one must be calculated separately.
	Vector2D temp_vect = Vector2D(points[1]->x - points[points.size()-1]->x, points[1]->y - points[points.size()-1]->y);
	temp_vect.makeNormalLeft();
	normals.push_back(temp_vect);
	
	// Return the array loaded with the normals
	return normals;
}