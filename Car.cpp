/**
 *  Car.cpp
 *  Brief
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 


// Included libs
#include "Car.h"


Car::Car(f16 pos_x, f16 pos_y):acc(0.5f), dec(0.25f), 
							max_vel(3.2f), step(PI/20),
							acc_reverse(0.3f), dec_reverse(0.2f),
							max_vel_reverse(-2.0f)
{
	dir = (PI/2); // North
	prev_dir = dir;
	vel = 0.0f; 
	reverse_mode = false;
	shape = new Rect(pos_x, pos_y, 6, 8);
}

Car::~Car()
{
	// Delete the Rect object instance
	delete shape;
}

void Car::handleInput(uint16_t prev_buttons, uint16_t cur_buttons)
{
	
	if(isBPressed(KEY_A, cur_buttons))
	{
		vel += acc;
		reverse_mode = false;
	}
	if(isBPressed(KEY_B, cur_buttons))
	{
		vel -= acc;
		reverse_mode = true;
	}
	
	if(isBPressed(KEY_RIGHT, cur_buttons) && !reverse_mode)
	{
		dir += step * (vel / max_vel); // Steer car depending on velocity
	}
	
	if(isBPressed(KEY_LEFT, cur_buttons) && !reverse_mode)
	{
		dir -= step * (vel / max_vel); // Steer car depending on velocity
	}
}

void Car::update()
{
	// Constantly decelerate the car
	if(vel > 0.4f && !reverse_mode)
	{
		vel -= dec;
	}
	else if( vel < -0.2f && reverse_mode)
	{
		vel += dec_reverse;
	}
	else
	{
		vel = 0;
	}
	
	// Cap velocity
	if( vel > max_vel)
	{
		vel = max_vel;
	}
	if( vel < max_vel_reverse)
	{
		vel = max_vel_reverse;
	}
	
	/**
	 *  Rotate the car only if the direction is different
	 *  than the previous one. Saves CPU
	 */
	if(dir != prev_dir)
	{
		f16 angle = dir - prev_dir;
		shape->rotatePoints(angle);
	}
	
	// Save the current direction
	prev_dir = dir;
	
	/**
	*  Keep the car within the bounds of the screen.
	*  Can be improved.
	*/
	if(shape->points[0]->x + shape->half_height + 1 >= SCREEN_WIDTH 
		|| shape->points[0]->x - shape->half_height - 1 <= 0 
		|| shape->points[0]->y + shape->half_height + 1 >= SCREEN_HEIGHT
		|| shape->points[0]->y - shape->half_height - 1 <= 0)	
	{
		// Make the car bounce back and stop it
		shape->addToCentrePosition(vel * cos(dir + PI), vel * sin(dir + PI));
		vel = -vel;
		/**
		 *  There is a bug in this part of code: when the car hits
		 *  the wall, it does stop and bounce back, but if the player
		 *  keeps steering and accelerating, eventually the car
		 *  will trespass the boundaries of the screen and the program
		 *  will get bugged.
		 */
	}
	
	/**
	 *  Move the car.
	 *  If the velocity is 0, there is no need to 
	 *  calculate sin and cos (they are expensive!)
	 */
	if(vel != 0.0f)
	{	
		shape->addToCentrePosition(vel * cos(dir), vel * sin(dir));
	}
	
	
	
	
	
}

void Car::draw()
{
	shape->drawFrame8(BLUE);
}