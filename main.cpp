/**
 *  main.cpp
 *  The main source file of the project.
 *  
 *  
 *  GTA: Vector City is a game for the GBA which 
 *  emulates one of the features of the first GTA:
 *  a controllable car seen from a top-down perspective.
 *  
 *  The collision-check functions are implemented
 *  here; the technique used for checking whether the 
 *  car is colliding with some other object is the 
 *  SAT (Separating Axis Theorem). 
 *  
 *  In the main are also defined the palette and the 
 *  buttons registers, which are subsequently passed
 *  to the car object for handling user input.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

 
 // Included libs
#include "Global.h"
#include <string.h>
#include <cstdio>
#include "font.h"
#include "Vector2D.h"
#include "Car.h"
#include "Ped.h"
#include <vector>


// Methods prototypes

/**
 *  Returns a std::vector container with vectors 
 *  starting from the origin (0,0) and pointing to
 *  each corner of the shape. This is needed to implement
 *  the SAT collision test.
 */ 
std::vector<Vector2D> PrepVectors(Rect *shape);

/**
 *  Returns a container (in this case is a Vector2D, because 
 *  it provides an easy and quick way of storing a couple of
 *  values which is available both to the main and to the 
 *  Rect class) with the max and min projection for a given
 *  shape (the shape is not an argument; instead its "prepared 
 *  vectors"(see PrepVectors) are the argument) on a given
 *  axis vector (*axis). This is the core of the SAT.
 *  
 */
Vector2D GetMinMaxShape(std::vector<Vector2D> *vect_shape, Vector2D *axis);

//void HandleCollisionsRectVSPoints(Rect *test_a, Vector2D *point);

/**
 *  Checks whether a given Rect object (the car basically) is
 *  colliding with the pedestrians, and behaves accordingly.
 *  It implements the SAT using the functions defined above.
 */
void HandleCollisionsRectVSPeds(Rect *test_a, std::vector<Ped*> * peds, const int32_t ped_num, int32_t &score);





// The entry point for the game
int main()
{
    // Put the display into bitmap mode 3, and enable background 2.
	REG_DISPCNT = MODE4 | BG2_ENABLE;

    // Set up the palette.
	SetPaletteBG(BLACK, black); // black
	SetPaletteBG(WHITE, white); // white
	SetPaletteBG(RED, red); // red
	SetPaletteBG(GREEN, green); // green
	SetPaletteBG(BLUE, blue); // blue
	SetPaletteBG(YELLOW, yellow);
	SetPaletteBG(B_PINK, bright_pink); // Bright pink
	
	// Palette currently displayed
	enum colorState
	{	
		MODE_0,
		MODE_1,
		MODE_2
	};
	
	// Frame counter
	uint32_t frames = 0;
	
	// Score counter
	int32_t score = 0;
	
	// Score string buffer
	char score_string[32];
	
	// Static message on top of screen
	char kill_string[100];
	snprintf(kill_string, 100, "KILLING SPREE!");
	// Set up a palette position for it, in order to make it flash
	SetPaletteBG(30, white);
	// Create its color state variable
	colorState kill_string_col_state = MODE_0; //WHITE
	
	// Previous & current buttons states
	static uint16_t prev_buttons = 0, cur_buttons = 0;
	
	// Create car instance
	Car *car = new Car(50,50);	
	
	// Pedestrian instances
	int32_t ped_number = 20; // Number of pedestrians
	std::vector<Ped*> peds; // Ped. entities container
	
	for(int32_t i = 1; i <= ped_number; i++)
	{
		// Create instances
		peds.push_back(new Ped(i, B_PINK, 2.0f));
	}
	
	// Main loop
	while (true)
	{
		// Flip the screen
		FlipBuffers();
		
		// Clear the screen
		ClearScreen8(BLACK);
		
		// Update frame counter
		frames ++;	
		
		
		// Get the current state of the buttons.
		cur_buttons = REG_KEYINPUT;
		
		// Handle Input
		car->handleInput(prev_buttons, cur_buttons);
		
		
		
		// Logic
		
		// Update the car
		car->update();
		
		// Update each pedestrian
		for(int32_t i = 0; i < ped_number; i++)
		{
			peds[i]->update();
			
			// If the current pedestrian was dead and has finished the death routine
			if(peds[i]->can_be_deleted)
			{
				// First delete the instance
				delete *(peds.begin() + i);
				// Then delete the element from the array
				peds.erase(peds.begin() + i);
				// Finally reduce the number of pedestrians
				ped_number --;
			}
		}
		
		// If there are no more pedestrians, quit
		if(ped_number == 0)
		{
			break;
		}
		
		// Handle eventual collisions
		HandleCollisionsRectVSPeds(car->shape, &peds, ped_number, score);
		
		// Update the score string
		snprintf(score_string, 32, "Score: %01d", score);
		
		// Update the message string
		if(frames % 2 == 0)
		{
			switch(kill_string_col_state)
			{
				case MODE_0: // WHITE
					kill_string_col_state = MODE_1;
					SetPaletteBG(30, red);
					break;
				case MODE_1: // RED
					kill_string_col_state = MODE_0;
					SetPaletteBG(30, white);
					break;
				default:;
			}
		}
	
	
		// Render objects on screen
		for(int32_t i = 0; i < ped_number; i++)
		{
			peds[i]->draw();
		}
		car->draw();
		drawString8(5, 5, WHITE, score_string, false);
		drawString8(110, 5, 30, kill_string, false);
		
		// Update previous buttons variable
		prev_buttons = cur_buttons;
		
		// VSync
		WaitVSync();
	}
	// Game over
	
	// Free memory
	delete car;
	peds.clear(); 	// Delete pedestrians instances

	// Clear the screen
	ClearScreen8(BLACK);
	
	// Flip the screen
	FlipBuffers();
		
	// Clear the screen again
	ClearScreen8(BLACK);
	
	
	
	// Show GOURANGA!s everywhere! (when the game is over)
	/**
	 *  Set a palette position for each GOURANGA!, so
	 *  that its colour can be alternated easily
	 */
	SetPaletteBG(50, white);
	SetPaletteBG(51, yellow);
	SetPaletteBG(52, green);
	SetPaletteBG(53, blue);
	SetPaletteBG(54, bright_pink);
	// Create its colour state variable
	colorState gouranga_string_state = MODE_0;
	
	// Set up the string
	char gouranga_string[10];
	snprintf(gouranga_string, 10, "GOURANGA!");
	
	// Draw 5 GOURANGAS strings
	drawString8(80, 70, 50, gouranga_string, false); // Centre
	drawString8(10, 10, 51, gouranga_string, false); // Top left
	drawString8(150, 10, 52, gouranga_string, false); // Top right
	drawString8(10, 130, 53, gouranga_string, false); // Bottom left
	drawString8(150, 130, 54, gouranga_string, false); // Bottom right
	
	// Flip the screen
	FlipBuffers();
	
	// Reuse the frames counter
	frames = 0;
	
	// Hang
	while(true)
	{
		// And make the GOURANGA strings change colours
		if(frames % 4 == 0)
		{
			switch(gouranga_string_state)
			{
				case MODE_0:
					gouranga_string_state = MODE_1;
					SetPaletteBG(50, yellow);
					SetPaletteBG(51, green);
					SetPaletteBG(52, blue);
					SetPaletteBG(53, bright_pink);
					SetPaletteBG(54, red);
					break;
				case MODE_1:
					gouranga_string_state = MODE_2;
					SetPaletteBG(50, white);
					SetPaletteBG(51, yellow);
					SetPaletteBG(52, green);
					SetPaletteBG(53, blue);
					SetPaletteBG(54, bright_pink);
					break;
				case MODE_2:
					gouranga_string_state = MODE_0;
					SetPaletteBG(50, green);
					SetPaletteBG(51, blue);
					SetPaletteBG(52, yellow);
					SetPaletteBG(53, red);
					SetPaletteBG(54, white);
					break;
				default:;
			}
		}		
		
		// Increment frames
		frames ++;
		
		WaitVSync();
	};
	
	// Exit
	return 0;
}

void HandleCollisionsRectVSPeds(Rect *test_a, std::vector<Ped*> * peds, const int32_t ped_num, int32_t &score)
{
	// Prepare the normals for the car
	std::vector<Vector2D> normals_a = test_a->getNormalsAsArray();
	
	/**
	 *  Create a container for holding the vectors 
	 *  used for collision check. These vectors are
	 *  originated in the origin (0,0) and they point
	 *  to the vertex of the shape (test_a).
	 */
	std::vector<Vector2D> vect_test_a = PrepVectors(test_a);
	
	// Get max and min projections for the OOBB (needed for SAT)
	Vector2D result_P1 = GetMinMaxShape(&vect_test_a, &normals_a[1]); // Axis 1
	Vector2D result_Q1 = GetMinMaxShape(&vect_test_a, &normals_a[0]); // Axis 2
	
	// Loop through the pedestrians vector and for each calculate projection on the axis
	for(int32_t i = 0; i < ped_num; i++)
	{
		// If the current pedestrian is alive
		if((*peds)[i]->alive)
		{
			// Create the projection vector of the current pedestrian
			Vector2D temp_vect_ped((*peds)[i]->pos->x, (*peds)[i]->pos->y);
			
			// Get projection of the point (pedestrian) (needed for SAT) on axis 1
			f16 result_point = temp_vect_ped.dotProduct(normals_a[1]);
			
			// If the two objects are not colliding on this axis
			if(result_P1.y < result_point || result_point < result_P1.x)
			{	
				// Skip the next part, as for SAT they are already considered not colliding
				continue;
			}
			// Get projection of the point (pedestrian) (needed for SAT) on axis 1
			result_point = temp_vect_ped.dotProduct(normals_a[0]);
			if(result_Q1.y < result_point || result_point < result_Q1.x)
			{
				// Skip the next part, as for SAT they are considered not colliding
				continue;
			}
			
			/**
			 *  If here, there is collision between the car (shape) and the 
			 *  current (i) pedestrian thus set the current hit ped to dead
			 */
			(*peds)[i]->setDead();
			// Increase score
			score += 100; 
		}
	}
}

// Returns a container with projection vectors for a given shape
std::vector<Vector2D> PrepVectors(Rect *shape)
{
	std::vector<Vector2D> vect;
	
	// Create vectors for projection and load them into the arrays
	for(int32_t i=0; i < 5; i++)
	{		
		// Get the point as a global one
		Point global_temp_point = shape->getPointGlobal(i);
		
		/**
		 *  Add vector to the array. The created array
		 *  has origin at (0,0) and its x(i) and y(j)
		 *  components are the current point's
		 *  coordinates.
		 */
		vect.push_back(Vector2D(global_temp_point.x, 
								global_temp_point.y));
	}
	
	// Return the loaded array
	return vect;
}

Vector2D GetMinMaxShape(std::vector<Vector2D> *vect_shape, Vector2D *axis)
{
	/**
	 *  Set initial minimum and maximum for 
	 *  the shape's projections. The projection is
	 *  achieved performing the dot product between
	 *  a given projection vector and the axis (the
	 *  Vector2D *axis)
	 */
	f16 min_proj = (*vect_shape)[1].dotProduct(*axis);
	f16 max_proj = (*vect_shape)[1].dotProduct(*axis);
	
	// Calculate max and min projectios by iterating along all of the corners
	for(uint32_t i = 2; i < vect_shape->size(); i ++)
	{
		// Create the current, temporary projection value
		f16 current_proj = (*vect_shape)[i].dotProduct(*axis);
		
		// Select minimum projection on axis
		if(current_proj < min_proj) // If current projection is smaller than the minimum one
			min_proj = current_proj;
		// Select maximum projection on axis
		if(current_proj > max_proj) // If current projection is greater than the minimum one
			max_proj = current_proj;
	}
	
	/**
	 *  Return a vector2D as it is a handy way 
	 *  for returning a couple of values
	 */
	return (Vector2D(min_proj, max_proj)); 
}


