/**
 *  Car.h
 *  Header file of Car class
 *  
 *  The car class defines a Car object, thus the controllable entity in the game.
 *  It is made of a Rect object (its shape), its color and vectors for handling
 *  movement.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef CAR_H
#define CAR_H

// Included libs
#include "Global.h"
#include "Utils.h"
#include "Vector2D.h"
#include "Rect.h"
#include "font.h"
#include <stdio.h>
#include <string.h>

// Class definition
class Car
{
	public:
		
		// Default constructor
		Car(f16 pos_x, f16 pos_y); 
		
		// Default deconstructor
		~Car(); 
		
		/**
		 *  Handle user input. Takes in the values of the registers from the main.
		 *  Can be improved.
		 */
		void handleInput(uint16_t prev_buttons, uint16_t cur_buttons);
		
		// Update the car's values
		void update();
			
		// Draw the car on screen
		void draw();
		
		
		Rect *shape; // Shape (and coordinates) of the car
		
	private:
	
		/**
		 *  Constants are used since they 
		 *  are stored into the external ROM, thus more
		 *  internal RAM free.
		 */
		const f16 acc; // Acceleration
		const f16 dec; // Deceleration
		const f16 max_vel; // Max velocity
		const f16 step; // How much (in radians) the car can steer
		const f16 acc_reverse;
		const f16 dec_reverse;
		const f16 max_vel_reverse;
		
		f16 dir, prev_dir; // Direction in radians
		f16 vel; // Magnitude of velocity
		coloursPalPos color; // Colour of the car
		bool reverse_mode; // Whether the car is in reverse. Can be improved using enums
};

#endif