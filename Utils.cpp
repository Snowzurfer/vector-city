/*
	Source file for the CUtils.h library.
	
	Coded by Alberto Taiuti
*/

// Included libs
#include "Utils.h"

void bresenhamLine8(f16 x1f, f16 y1f, f16 x2f, f16 y2f, int col)
{
	// Switch fixed points to normal integers
	int x1 = fixedpoint::fix2int<16>(x1f); 
	int x2 = fixedpoint::fix2int<16>(x2f); 
	int y1 = fixedpoint::fix2int<16>(y1f); 
	int y2 = fixedpoint::fix2int<16>(y2f);
	
    int delta_x(x2 - x1);
    // if x1 == x2, then it does not matter what we set here
    signed char const ix((delta_x > 0) - (delta_x < 0));
    delta_x = std::abs(delta_x) << 1;
 
    int delta_y(y2 - y1);
    // if y1 == y2, then it does not matter what we set here
    signed char const iy((delta_y > 0) - (delta_y < 0));
    delta_y = std::abs(delta_y) << 1;
 
     PlotPixel8(x1, y1, col);
 
    if (delta_x >= delta_y)
    {
        // error may go below zero
        int error(delta_y - (delta_x >> 1));
 
        while (x1 != x2)
        {
            if ((error >= 0) && (error || (ix > 0)))
            {
                error -= delta_x;
                y1 += iy;
            }
            // else do nothing
 
            error += delta_y;
            x1 += ix;
 
            PlotPixel8(x1, y1, col);
        }
    }
    else
    {
        // error may go below zero
        int error(delta_x - (delta_y >> 1));
 
        while (y1 != y2)
        {
            if ((error >= 0) && (error || (iy > 0)))
            {
                error -= delta_y;
                x1 += ix;
            }
            // else do nothing
 
            error += delta_x;
            y1 += iy;
 
            PlotPixel8(x1, y1, col);
        }
    }
}

bool isBPressed(uint16_t button, uint16_t reg)
{
	if((reg & button) == 0)
		return true;
	else
		return false;
}

bool isBJustPressed(uint16_t button, uint16_t prevReg, uint16_t reg)
{
	// If the button is pressed now, but before was not
	if((isBPressed(button, reg) == true) && 
		(isBPressed(button, prevReg) == false))
		return true;
	else
		return false;
	
}

void drawChar8(int32_t x, int32_t y, int32_t colour, char c, bool bold)
{
	for (int yo = 0; yo < 8; yo++) 
	{
		for (int xo = 0; xo < 8; xo++) 
		{
		
			if(bold && font_bold[int(c)][(yo * 8) + xo])
			{
				PlotPixel8(x + xo, y + yo, colour);
			}
			else if(font_medium[int(c)][(yo * 8) + xo])
			{
				PlotPixel8(x + xo, y + yo, colour);
			}
			
		}
	}
}

void drawString8(int32_t x, int32_t y, int32_t colour, const char *s, bool bold)
{
	while (*s != '\0') 
	{
		drawChar8(x, y, colour, *s, bold);
		x += 8;
		s++;
	}
}

void drawExplosion(int32_t x, int32_t y, int32_t part_num, int32_t radius, int32_t col)
{
	// Variables used to store offsets
	int32_t xOff = 0, yOff = 0;
	uint32_t temp = (2*radius)+1;
	
	for(int i = 0; i < part_num; i++)
	{
		// Generate offsets
		xOff = rand()%temp;
		xOff -= radius;
		yOff = rand()%temp;
		yOff -= radius;
		
		// Circular explosion
		if(((xOff*xOff) + (yOff*yOff)) < (radius * radius))
		{
			PlotPixel8(x + xOff, y + yOff, col);
		}
	}
}
