/**
 *  point.h
 *  Header file of Point class.
 *  
 *  The point class represents a geometric point, and contains
 *  useful functions for manipulating it.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */

#ifndef POINT_H
#define POINT_H

// Included libs
#include "Global.h"

// Class definition
class Point
{
	public:
		
		//Default constructor
		Point();
		
		// 2nd constructor
		Point(f16 x, f16 y);
		
		// Default deconstructor
		~Point(); 

		f16 x, y; // Position of the point
		
		// Rotate around the origin by a given angle (angle)
		void rotate2D(f16 angle);
		
		// Set a new position (default: same as before)
		void setPos(f16 new_x, f16 new_y);
		
		// Increment (or decrement) the position by x_step, y_step
		void addToPos(f16 x_step, f16 y_step);		
};

#endif