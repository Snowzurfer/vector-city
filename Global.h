/**
 *  Global.h
 *  Header file containing declarations and includes
 *  needed in the whole project.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef GLOBAL_H
#define GLOBAL_H

// Included libs
#include <stdint.h>
#include <cstdlib>
#include "fixed_class.h"
#include "gba.h"

// Position of colours inside the palette, for sanity sake
enum coloursPalPos 
{
	BLACK,
	WHITE,
	RED,
	GREEN,
	BLUE,
	YELLOW,
	B_PINK
};

#define PI 3.14159265

// Define a type for the 16.16 fixed point type for ease of writing
typedef fixedpoint::fixed_point<16> f16;

#endif