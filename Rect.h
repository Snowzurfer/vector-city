/**
 *  Rect.h
 *  Header file of Rect class.
 *  
 *  The Rect class represents a geometric rectangle. It is made of 4 points
 *  (its vertex), stored under an std::vector container.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef RECT_H
#define RECT_H

// Included libs
#include "Global.h"
#include "Utils.h"
#include "Vector2D.h"
#include "Point.h"
#include <vector>

// Class definition
class Rect
{
	public:
		
		// Default constructor
		Rect(f16 centerX, f16 centerY, f16 _width, f16 _height); 
		
		// Default deconstructor
		~Rect(); 
		
		// Draw a frame using the coordinates of the rectangle
		void drawFrame8(coloursPalPos col);
		
		// Rotate the vertices around the centre by a given angle
		void rotatePoints(f16 angle);
		
		// Increment (or decrement) the centre coordinates by x_step, y_step
		void addToCentrePosition(f16 x, f16 y);
		
		// Returns the a given point of the shape (a vertex) (point_num) under global coordinates
		Point getPointGlobal(int32_t point_num);
		
		// Returns a vector containing the normals of the shape
		std::vector<Vector2D> getNormalsAsArray();
		
		std::vector<Point*> points; // Vector containing pointers to instances of the points composing the rectangle
		typedef std::vector<Point*>::iterator p_itor; // Iterator for the specified vector
		
		/**
		 *  Constants are used since they 
		 *  are stored into the external ROM, thus more
		 *  internal RAM free.
		 */
		const f16 width;
		const f16 height;
		const f16 half_width;
		const f16 half_height;
};

#endif