/**
 *  Ped.cpp
 *  Source file of the Ped class
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#include "Ped.h"


Ped::Ped(int32_t seed, int32_t color, f16 vel):
			step(PI/4), part_num(40)
{
	this->color = color;
	this->vel = vel;
	timer = 0;
	dir = PI/2; // 90 degrees
	srand(seed); // Seed the rand function
	pos = new Point(rand() % 230 + 5, rand() % 150 + 5); // Position the pedestrian randomly
	seed_increment = seed; // Seed seed_increment using the parameter seed
	alive = true;
	can_be_deleted = false;
	splat_generated = false;
}

Ped::~Ped()
{
	// Delete the position instance
	delete pos;
	delete [] splatter_points;
}

void Ped::update()
{
	if(!alive)
	{
		// When the routine is over
		if((timer - death_routine_timer) > 120)
		{
			// Tell the main to delete it
			can_be_deleted = true;
		}
	}
	else
	{
		/**
		 *  Change the pedestrian direction 
		 *  every 30 calls. Can be improved by
		 *  making the pedestrian change direction
		 *  at a random time-frame.
		 */
		if(timer % 30 == 0)
		{
			// Seed the rand function
			srand(seed_increment);

			/**
			 *  Save the random generated number, and use it to seed the next time.
			 *  This expedient is used since there's no time lib on the GBA for
			 *  seeding the rand function the classic way.
			 */
			seed_increment = rand() % 7 + 1;
			
			// With a step of PI/4 within a range of max 7 steps of (PI/4)
			dir += (step * (seed_increment));
		}
		
		// Move the pedestrian using vector math
		pos->addToPos(vel * cos(dir), vel * sin(dir));
		
		/**
		 *  Keep the pedestrian within the bounds of the screen.
		 *  Can be improved.
		 */
		if((pos->x >= SCREEN_WIDTH) || (pos->x <= 0))
		{
			dir += PI;
		}
		if((pos->y >= SCREEN_HEIGHT) || (pos->y <= 0))
		{
			dir += PI;
		}
	}
	
	// Update internal timer
	timer ++;
}

Point* Ped::drawSplatPed(int32_t x, int32_t y, int32_t part_num, int32_t radius, int32_t col)
{
	// Variables used to store offsets
	int32_t xOff = 0, yOff = 0;
	uint32_t diameter = (2*radius)+1;
	
	/**
	 *  Create an array of Points, which will be used to store the
	 *  values of the plotted pixels; this way, the splatter
	 *  will remain the same each frame from the call of the 
	 *  function onwards.
	 */
	Point *plotted_points = new Point[part_num];
	
	
	for(int i = 0; i < part_num; i++)
	{
		// Generate offsets and cap them inside the diameter
		xOff = rand()%diameter;
		xOff -= radius;
		yOff = rand()%diameter;
		yOff -= radius;
		
		// Circular explosion
		if(((xOff*xOff) + (yOff*yOff)) < (radius * radius))
		{
			PlotPixel8(x + xOff, y + yOff, col);
			plotted_points[i].x = x + xOff;
			plotted_points[i].y = y + yOff;
		}
	}
	
	// Make the object know the splatter has been created
	splat_generated = true;
	
	return plotted_points;
}

void Ped::draw()
{	
	if(!alive)
	{
		if(!splat_generated)
		{
			// Draw the splatter
			splatter_points = drawSplatPed(fixedpoint::fix2int(pos->x), fixedpoint::fix2int(pos->y),
							part_num, 10, RED);	
		}
		else
		{
			for(int32_t i = 0; i < part_num; i ++)
			{
				PlotPixel8(fixedpoint::fix2int<16>(splatter_points[i].x)
							,fixedpoint::fix2int<16>(splatter_points[i].y)
							, RED);
			}
		}
	}
	else
	{
		/**
		 *  Use gba.h's plot pixel function by transforming the fixeds.
		 *  They would have been cast to INT anyway.
		 */
		PlotPixel8(fixedpoint::fix2int<16>(pos->x), 
					fixedpoint::fix2int<16>(pos->y), color);
	}
}

void Ped::setDead()
{
	alive = false;
	death_routine_timer = timer;
}