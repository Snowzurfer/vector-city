/**
 *  Ped.h
 *  Header file for Ped class.
 *  
 *  This class represents a pedestrian in the game. The pedestrian
 *  entity is free to move around within the boundaries
 *  of the GBA's screen. Its position is given by a Point object
 *  and its initial position is randomly chosen.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef PED_H
#define PED_H

// Included libs
#include "Global.h"
#include "Utils.h"
#include "Vector2D.h"
#include "Point.h"
#include <ctime>

// Class definition
class Ped 
{
	public:
	
		/**
		 *  Default constructor. It randomly positions
		 *  the pedestrian using the seed (seed).
		 */
		Ped(int32_t seed, int32_t color, f16 vel); 
		
		// Default deconstructor
		~Ped();
		
		// Update the pedestrian's values
		void update(); 
			
		// Draw the entity on screen
		void draw(); 
		
		/**
		 *  When the pedestrian is being run over by the car,
		 *  it dies. An explosion must be shown on screen, so
		 *  it is set to dead and runs a routine for a certain
		 *  timespan. When the routine is finished, it returns
		 *  TRUE and the main can then delete this pedestrian
		 *  instance.
		 */
		void setDead();
		

		Point *pos; // Coordinates of the pedestrian
		bool alive; // Wether this ped has been killed yet or not
		bool can_be_deleted; // When the death routine is finished, set this to true
		
		
	private:
		
		/**
		 *  Constants are used since they 
		 *  are stored into the external ROM, thus more
		 *  internal RAM free.
		 */
		const f16 step; // How much (in radians) the pedestrian can "steer"
		f16 dir; // Direction in radians
		f16 vel; // Magnitude of velocity
		int32_t color; // Colour of the pedestrian
		uint32_t timer; // Internal timer
		uint32_t death_routine_timer; // Amount of time to run the death routine
		int seed_increment; // Internal seed variable
		bool splat_generated; // If the splat was generated, this is set to true
		const int32_t part_num; // Number of particles for the blood splatter
		Point *splatter_points; // Pointer to array of points representing the splatter
		
		/**
		 *  Modified version of the function in "Utils.h". This one returns
		 *  an array of points where these are the positions of the particles
		 *  generated. This way they can be plotted again easily.
		 */
		Point* drawSplatPed(int32_t x, int32_t y, int32_t part_num, int32_t radius, int32_t col);
};

#endif