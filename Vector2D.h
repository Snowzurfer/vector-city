/**
 *  Vector2D.h
 *  Header file of Vector2D class.
 *  
 *  The Vector2D class represents a 2D vector and
 *  it is much similar to the Point class, with the
 *  difference that it is treated and developed as a proper vector.
 *  Moreover, it contains methods inherent to vector maths.
 *  
 *  Project: GTA: Vector City
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef VECTOR2D_H
#define VECTOR2D_H

// Included libs
#include "Global.h"
#include <cmath>

// Class definition
class Vector2D
{
	public:
	
		// Default constructor
		Vector2D(f16 x, f16 y); 
		
		// Default deconstructor
		~Vector2D(); 
		
		f16 x, y; // X(i) and Y(j) components
		f16 magnitude; // Magnitude of the vector	
		f16 angle; // Angle of vector with the X-axis
		
		// Rotate around the origin by a given angle (angle)
		void rotate2D(f16 angle);
		
		// Set new positions (default: same as before)
		void setPos(f16 new_x, f16 new_y);
		
		// Increment (or decrement) the components by x_step, y_step
		void addToPos(f16 x_step, f16 y_step);
		
		// Calculate the dot product between current vector and another vector (b)
		f16 dotProduct(const Vector2D &b);
		
		// Normalize the current vector
		void normalize();
		
		// Transform the current vector to its left normal
		void makeNormalLeft();		
};

#endif